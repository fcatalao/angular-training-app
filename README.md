# TrainingApp

```
Install angular-cli and create an app
- [X] sudo npm install -g @angular/cli
- [X] ng new training-app (routing/scss)
- [X] cd training-app
- [X] show app structure

Create module with routing and component
- [X] ng g module guitar --routing=true
- [X] ng g component guitar/strings

Build the app to prod
- [X] npm run build
- [X] update build command —prod

Run unit and e2e tests
- [X] npm run test
- [X] update test command —code-coverage
- [X] npm run e2e

Lazy loading components
- [X] add lazy loading route to guitar/strings component

Schematics: PWA applications
- [X] npm start (show offline behaviour)
- [X] ng add @angular/pwa
- [X] npm install -g http-server && http-server -p 8080 -c-1 dist/training-app

Schematics: Add Angular Material (Navigation and Dashboard)
- [X] https://material.angular.io/guide/schematics
- [X] ng add @angular/material
- [X] ng generate @angular/material:nav my-nav
- [X] ng generate @angular/material:dashboard my-dashboard
- [X] update app-routing module to load dashboard component

Schematics: Ng-Bootstrap
- [X] https://ng-bootstrap.github.io/
- [X] https://youtu.be/tybcHBQIpwk

Prettier + Tslint + Airbnb
- [X] install prettier (https://prettier.io/)
- [X] npx prettier --write src/**/*
- [X] ignore tslint formatting rulers (https://github.com/prettier/tslint-config-prettier)
- [X] import airbnb styleguide rules (https://github.com/progre/tslint-config-airbnb)

Schematics: AngularFire
- [X] create new app on https://firebase.google.com/
- [X] ng add @angular/fire@next
- [X] add deploy script to package.json: "ng run training-app:deploy"
```

